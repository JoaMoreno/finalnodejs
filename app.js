//Var------->
require("dotenv").config();
path = require("path");

const port = process.env.PORT;
const express = require("express");
const corsMiddleware = require("./middlewares/cors");
const dbConection = require("./configs/mongoose");
const createAdmin = require("./libs/initialSetup");

const { validateToken } = require("./middlewares/auth.validate");

const userRoutes = require("./routes/user.routes");
const clothingRoutes = require("./routes/clothing.routes");
const authRoutes = require("./routes/auth.routes");

const app = express();
app.use(corsMiddleware);

//Crea el primer admin
createAdmin();

(fs = require("fs")), (morgan = require("morgan"));
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "access.log"),
  { flags: "a" }
);

//Coneccion con la DB
dbConection();

app.use(express.json());

app.use(
  morgan("dev", {
    skip: function (req, res) {
      return res.statusCode < 400;
    },
  })
);

app.use(
  morgan(
    "Date: [:date[clf]] :http-version |(:method) |:response-time ms |':url' => :status",
    {
      stream: accessLogStream,
    }
  )
);

//Routes
app.use(validateToken);
app.use("/api/user", userRoutes);
app.use("/api/clothing", clothingRoutes);
app.use("/api/auth", authRoutes);

app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
