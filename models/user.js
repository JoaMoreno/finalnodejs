const { Schema, model } = require("mongoose");

const UserSchema = Schema(
  {
    nombre: {
      type: String,
      required: true,
    },
    apellido: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    rol: {
      type: String,
      enum: ["ADMIN", "EMPLOYEE", "USER"],
      default: "USER",
    },
  },
  {
    timestamps: true,
  }
);

UserSchema.methods.toJSON = function () {
  const { password, createdAt, updatedAt, __v, ...user } = this.toObject();
  return user;
};

module.exports = model("User", UserSchema);
