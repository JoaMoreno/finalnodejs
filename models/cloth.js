const { Schema, model } = require("mongoose");

const ClothSchema = Schema(
  {
    tipo: {
      type: String,
      required: true,
      enum: ["BUZO", "REMERA", "CAMPERA", "PANTALÓN"],
    },
    cantidad: {
      type: Number,
      required: true,
    },
    precio: {
      type: Number,
      required: true,
    },
    descripcion: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("Cloth", ClothSchema);
