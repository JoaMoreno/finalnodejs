const User = require("../models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const register = async (req, res) => {
  const { password, ...rest } = req.body;
  rest.password = bcrypt.hashSync(password, 10);
  try {
    const user = await User.create(rest);
    return res.status(201).json({
      code: "SUCCESSFULLY CREATED",
      message: null,
      success: true,
      data: user,
    });
  } catch (err) {
    return res.status(500).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ email });
    const passwordValidate = bcrypt.compareSync(password, user.password);
    if (!user || !passwordValidate) {
      return res.status(401).send({
        code: "AUTH-ERR",
        message: "Invalid credentials",
        success: false,
        data: null,
      });
    }
    const token = jwt.sign(
      { nombre: user.nombre, _id: user._id },
      process.env.TOKEN_SECRET,
      { expiresIn: "1h" }
    );
    return res.status(200).json({
      code: "SUECCESSFULLY LOGGED IN",
      message: null,
      success: true,
      data: { user, token },
    });
  } catch (err) {
    return res.status(400).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

module.exports = { login, register };
