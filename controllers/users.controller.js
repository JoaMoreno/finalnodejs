const User = require("../models/user");

const getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params._id);
    if (!user) {
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: true,
        data: null,
      });
    }
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: user,
    });
  } catch (err) {
    return res.status(400).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const delUser = async (req, res) => {
  try {
    const user = await User.findById(req.params._id);
    if (!user) {
      return res.status(404).json({
        code: "USER NOT-FOUND",
        message: null,
        success: true,
        data: null,
      });
    }
    await user.deleteOne();
    return res.status(200).json({
      code: "USER DELETED",
      message: null,
      success: true,
      data: null,
    });
  } catch (err) {
    return res.status(400).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const putUser = async (req, res) => {
  try {
    const user = await User.findOneAndUpdate(
      { _id: req.params._id },
      { ...req.body },
      { new: true }
    );
    if (!user) {
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: user,
      });
    }
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: user,
    });
  } catch (err) {
    return res.status(500).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

module.exports = { getUser, putUser, delUser };
