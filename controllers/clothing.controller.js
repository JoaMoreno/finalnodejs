const Cloth = require("../models/cloth");

const getClothing = async (req, res) => {
  try {
    const clothing = await Cloth.findById(req.params._id);
    if (!clothing) {
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null,
      });
    }
    return res.status(200).json({
      code: "PRODUCT FOUND",
      message: null,
      success: true,
      data: clothing,
    });
  } catch (err) {
    return res.status(500).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const postClothing = async (req, res) => {
  try {
    const clothing = await Cloth.create(req.body);
    return res.status(201).json({
      code: "SAVED PRODUCT",
      message: null,
      success: true,
      data: clothing,
    });
  } catch (err) {
    return res.status(500).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const putClothing = async (req, res) => {
  try {
    const clothing = await Cloth.findOneAndUpdate(
      { _id: req.params._id },
      { ...req.body },
      { new: true }
    );
    if (!clothing) {
      return res.status(400).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null,
      });
    }
    return res.status(200).json({
      code: "EDITED PRODUCT",
      message: null,
      success: true,
      data: clothing,
    });
  } catch (err) {
    return res.status(500).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const delClothing = async (req, res) => {
  try {
    const cloth = await Cloth.findById(req.params._id);
    if (!cloth) {
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: true,
        data: null,
      });
    }
    await cloth.deleteOne();
    return res.status(200).json({
      code: "REMOVED PRODUCT",
      message: null,
      success: true,
      data: null,
    });
  } catch (err) {
    return res.status(500).send({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

module.exports = { getClothing, postClothing, putClothing, delClothing };
