const { Router } = require("express");
const router = Router();

const { login, register } = require("../controllers/auth.controller");
const {
  validateLogin,
  validateRegister,
} = require("../middlewares/auth.validate");

router.post("/login", validateLogin, login);
router.post("/register", validateRegister, register);

module.exports = router;
