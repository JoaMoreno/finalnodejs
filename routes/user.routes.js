const { Router } = require("express");
const router = Router();
const {
  putUser,
  getUser,
  delUser,
} = require("../controllers/users.controller");

const { validatePutUser, isSameUser } = require("../middlewares/user.validate");
const validateIdParam = require("../middlewares/_id.validate");
const { validateRoles } = require("../middlewares/auth.validate");

router.get(
  "/:_id",
  [validateRoles("ADMIN", "USER"), validateIdParam, isSameUser],
  getUser
);
router.put(
  "/:_id",
  [
    validateRoles("ADMIN", "USER"),
    validateIdParam,
    validatePutUser,
    isSameUser,
  ],
  putUser
);
router.delete(
  "/:_id",
  [validateRoles("ADMIN", "USER"), validateIdParam, isSameUser],
  delUser
);

module.exports = router;
