const joi = require("joi");

const validatePutUser = async (req, res, next) => {
  const schemaUser = joi
    .object({
      nombre: joi.string(),
      apellido: joi.string(),
      password: joi.string(),
      email: joi.string(),
      rol: joi.string().valid("ADMIN", "USER", "EMPLOYEE"),
    })
    .required();
  try {
    await schemaUser.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: err.details[0].message,
      success: false,
      data: null,
    });
  }
};

//Valida que sea el mismo usuario el que quiere realizar la operacion

const isSameUser = async (req, res, next) => {
  const logedRol = req.user.rol;
  const bodyRol = req.body.rol;
  const tokenUserId = req.user._id;
  if (logedRol === "USER") {
    if (tokenUserId != req.params._id || bodyRol) {
      return res.status(400).json({
        code: "VALIDATION-ERR",
        message: "UNAOUTHORIZED",
        success: false,
        data: null,
      });
    }
  }
  next();
};

module.exports = { validatePutUser, isSameUser };
