const joi = require("joi");

const validatePostClothing = async (req, res, next) => {
  const schemaClothing = joi
    .object({
      tipo: joi
        .string()
        .valid("BUZO", "REMERA", "CAMPERA", " PANTALÓN")
        .required(),
      cantidad: joi.number().required(),
      precio: joi.number().required(),
      descripcion: joi.string().required(),
    })
    .required();
  try {
    await schemaClothing.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: err.details[0].message,
      success: false,
      data: null,
    });
  }
};

const validatePutClothing = async (req, res, next) => {
  const schemaClothing = joi
    .object({
      tipo: joi.string().valid("BUZO", "REMERA", "CAMPERA", " PANTALÓN"),
      cantidad: joi.number(),
      precio: joi.number(),
      descripcion: joi.string(),
    })
    .required();
  try {
    await schemaClothing.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: err.details[0].message,
      success: false,
      data: null,
    });
  }
};

module.exports = { validatePostClothing, validatePutClothing };
