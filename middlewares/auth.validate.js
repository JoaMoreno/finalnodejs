const jwt = require("jsonwebtoken");
const joi = require("joi");
const User = require("../models/user");

const validateToken = async (req, res, next) => {
  if (req.url === "/api/auth/login" || req.url === "/api/auth/register") {
    return next();
  }

  const auth = req.headers.authorization;
  if (!auth) {
    return res.status(401).json({
      code: "AUTH-ERR",
      message: "NO TOKEN PROVIDEN",
      success: false,
      data: null,
    });
  }
  try {
    const token = auth.split(" ")[1];
    const { _id } = jwt.verify(token, process.env.TOKEN_SECRET);
    const user = await User.findById({ _id });
    if (!user) {
      return res.status(404).send({
        code: "AUTH-ERR",
        message: "USER NOT FOUND",
        success: false,
        data: null,
      });
    }
    req.user = user;
    return next();
  } catch (err) {
    return res.status(400).json({
      code: "AUTH-ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const validateRegister = async (req, res, next) => {
  const schemaUser = joi
    .object({
      nombre: joi.string().required(),
      apellido: joi.string().required(),
      password: joi.string().required(),
      email: joi.string().required(),
    })
    .required();
  try {
    await schemaUser.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: err.details[0].message,
      success: false,
      data: null,
    });
  }
};

const validateLogin = async (req, res, next) => {
  const schemaUser = joi
    .object({
      password: joi.string().required(),
      email: joi.string().required(),
    })
    .required();
  try {
    await schemaUser.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({
      code: "VALIDATION-ERR",
      message: err.details[0].message,
      success: false,
      data: null,
    });
  }
};

const validateRoles = (...roles) => {
  return async (req, res, next) => {
    if (!roles.includes(req.user.rol)) {
      return res.status(403).json({
        code: "AUTH-ERR",
        message: "UNAUTHORIZED",
        success: false,
        data: null,
      });
    }
    return next();
  };
};

module.exports = {
  validateToken,
  validateLogin,
  validateRegister,
  validateRoles,
};
