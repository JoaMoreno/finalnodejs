const mongoose = require("mongoose");

const dbConection = async () => {
  try {
    await mongoose.connect(
      `mongodb+srv://${process.env.USER_DB}:${process.env.PASS_DB}@ayiacademy.4efdb.mongodb.net/${process.env.DB}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    );
    console.log("DB-Conectada");
  } catch (err) {
    console.log(err);
  }
};

module.exports = dbConection;
